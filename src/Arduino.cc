#include "Arduino.h"

/*!
************************************************
* Arduino constructor.
*/
Arduino::Arduino(std::string model, const pugi::xml_node configuration) : fModel(model), fConfiguration(configuration) { ; }

/*!
************************************************
* Arduino distructor.
************************************************
*/
Arduino::~Arduino(void) {}

void Arduino::setParameter(std::string parameter, float value, std::string component) {}
// void Arduino::setParameter(std::string parameter, bool value,        std::string component){}
void Arduino::setParameter(std::string parameter, int value, std::string component) {}
void Arduino::setParameter(std::string parameter, std::string value, std::string component) {}

float       Arduino::getParameterFloat(std::string parameter, std::string component) { return 0; }
int         Arduino::getParameterInt(std::string parameter, std::string component) { return 0; }
bool        Arduino::getParameterBool(std::string parameter, std::string component) { return false; }
std::string Arduino::getParameterString(std::string parameter, std::string component) { return ""; }

std::vector<std::string> Arduino::getStatusList(void) const { return std::vector<std::string>(); }
std::vector<std::string> Arduino::getComponentList(void) const { return std::vector<std::string>(); }
std::string              Arduino::getModel() const { return fModel; }
std::string              Arduino::getID(void) const { return fId; }

std::string Arduino::convertToLFCR(std::string strXML)
{
    if(!strXML.compare("CR") || !strXML.compare("\\r"))
        return "\r";
    else if(!strXML.compare("LF") || !strXML.compare("\\n"))
        return "\n";
    else if(!strXML.compare("CRLF") || !strXML.compare("\\r\\n"))
        return "\r\n";
    else if(!strXML.compare("LFCR") || !strXML.compare("\\n\\r"))
        return "\n\r";
    else
    {
        std::stringstream error;
        error << "Arduino configuration: string " << strXML
              << " is not understood, please check keeping in mind that the only available options are: \"\\n\" \"\\r\" \"\\n\\r\" \"\\r\\n\" \"LF\" \"CR\" \"CRLF\" \"LFCR\", aborting...";
        throw std::runtime_error(error.str());
    }
}

/*!
************************************************
************************************************
*/