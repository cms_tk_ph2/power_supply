#include "Rigol.h"
#include "EthernetConnection.h"
#include "SerialConnection.h"
#include <iostream>
#include <string>
#include <unistd.h>

Rigol::Rigol(const pugi::xml_node configuration) : PowerSupply("Rigol", configuration) { configure(); }
Rigol::~Rigol()
{
    if(fConnection != nullptr) delete fConnection;
}

void Rigol::configure()
{
    std::cout << "Configuring Rigol ..." << std::endl;
    std::string connectionType = fConfiguration.attribute("Connection").as_string();
    int         timeout        = fConfiguration.attribute("Timeout").as_int();
    fSeriesName                = fConfiguration.attribute("Series").as_string();
    std::cout << connectionType << " connection ..." << std::endl;
    if(fSeriesName != "DP800" && fSeriesName != "DP900")
    {
        std::stringstream error;
        error << "Rigol configuration: the series named " << fSeriesName << " has no code implemented, aborting...";
        throw std::runtime_error(error.str());
    }
    if(std::string(fConfiguration.attribute("Connection").value()).compare("Serial") == 0)
    {
        std::string port        = fConfiguration.attribute("Port").as_string();
        int         baudRate    = fConfiguration.attribute("BaudRate").as_int();
        bool        flowControl = fConfiguration.attribute("FlowControl").as_bool();
        std::string parity      = fConfiguration.attribute("Parity").as_string();
        bool        removeEcho  = fConfiguration.attribute("RemoveEcho").as_bool();
        std::string terminator  = fConfiguration.attribute("Terminator").as_string();
        std::string suffix      = fConfiguration.attribute("Suffix").as_string();
        terminator              = PowerSupply::convertToLFCR(terminator);
        suffix                  = PowerSupply::convertToLFCR(suffix);
        fConnection             = new SerialConnection(port, baudRate, flowControl, parity, removeEcho, terminator, suffix, timeout);
    }
    else if(std::string(fConfiguration.attribute("Connection").value()).compare("Ethernet") == 0)
    {
        fConnection = new SharedEthernetConnection(fConfiguration.attribute("IPAddress").value(), std::stoi(fConfiguration.attribute("Port").value()));
    }
    else
    {
        std::stringstream error;
        error << "Rigol: Cannot implement connection type " << fConfiguration.attribute("Connection").value() << ".\n"
              << "Possible values are Serial or Ethernet";
        throw std::runtime_error(error.str());
    }

    for(pugi::xml_node channel = fConfiguration.child("Channel"); channel; channel = channel.next_sibling("Channel"))
    {
        std::string inUse = channel.attribute("InUse").value();
        if(inUse.compare("Yes") != 0 && inUse.compare("yes") != 0) continue;
        std::string id = channel.attribute("ID").value();
        std::cout << __PRETTY_FUNCTION__ << "Configuring channel: " << id << std::endl;
        PowerSupply::fChannelMap.emplace(id, new RigolChannel(fConnection, channel, fSeriesName));
    }
}

RigolChannel::RigolChannel(Connection* connection, const pugi::xml_node configuration, std::string seriesName)
    : PowerSupplyChannel(configuration), fConnection(connection), fChannel(fConfiguration.attribute("Channel").value()), fSeriesName(seriesName)
{
}
RigolChannel::~RigolChannel() {}

/*!
************************************************
 * Sends write command to connection.
 \param command Command to be send.
************************************************
*/
void RigolChannel::write(std::string command) { fConnection->write(command); }

/*!
************************************************
 * Sends read command to connection.
 \param command Command to be send.
************************************************
*/
std::string RigolChannel::read(std::string command)
{
    int repeat=3; //three attempts
    while(repeat>0) {
        repeat--;
        try{
            std::string answer = fConnection->read(command);
            int i = answer.find("\n");
            if ( i >= 0) {
        	    return  answer.substr(0,i);
            }
            std::cout << answer << std::endl;
        return answer;
        }
        catch(std::runtime_error &e ){
            std::cout << "No answerm trying again in 1sec" << std::endl;
            sleep(1);
        }
    }
    return "";
}

void RigolChannel::turnOn()
{
    write(std::string(":OUTP ") + fChannel + ",ON");
    // std::cout << "Turn on channel " << fConfiguration.attribute("Channel").value() << " output." << std::endl;
}

void RigolChannel::turnOff()
{
    write(std::string(":OUTP ") + fChannel + ",OFF");
    // std::cout << "Turn off channel " << fConfiguration.attribute("Channel").value() << " output." << std::endl;
}

bool RigolChannel::isOn()
{
    std::string answer = read("OUTP? " + fChannel);
    int result;
    if(fSeriesName == "DP800")
        result = ( answer.substr(0, 2) == "ON"); 
    else if(fSeriesName == "DP900")
        sscanf(answer.c_str(), "%d", &result);
    else
    {
        std::stringstream error;
        error << "Rigol isOn: command not implemented for " << fSeriesName << ", aborting ...";
        throw std::runtime_error(error.str());
    }

    std::cout << " Rigol " << fSeriesName << " isOn result " << result << std::endl;
    return result;
}

void RigolChannel::setVoltage(float voltage) { write(":APPL " + fChannel + "," + std::to_string(voltage)); }

void RigolChannel::setCurrent(float current) { setCurrentCompliance(current); }

float RigolChannel::getOutputVoltage()
{
    std::string answer = read(":MEAS:VOLT? " + fChannel);
    float       result = 0;
    if(answer.rfind("OFF") == std::string::npos) { sscanf(answer.c_str(), "%f", &result); }
    return result;
}

float RigolChannel::getSetVoltage() { return getVoltageCompliance(); }

float RigolChannel::getCurrent()
{
    std::string answer = read(":MEAS:CURR? " + fChannel);
    float       result = 0;
    if(answer.rfind("OFF") == std::string::npos) { sscanf(answer.c_str(), "%f", &result); }
    return result;
}

void RigolChannel::setVoltageCompliance(float voltage) { setVoltage(voltage); }

void RigolChannel::setCurrentCompliance(float current) { write(":APPL " + fChannel + "," + std::to_string(getSetVoltage()) + "," + std::to_string(current)); }

float RigolChannel::getVoltageCompliance()
{
    std::string answer = read(":APPL? " + fChannel + ",VOLT");
    float       result = 0;
    if(answer.rfind("OFF") == std::string::npos) { sscanf(answer.c_str(), "%f", &result); }
    return result;
}

float RigolChannel::getCurrentCompliance()
{
    std::string answer = read(":APPL? " + fChannel + ",CURR");
    float       result = 0;
    if(answer.rfind("OFF") == std::string::npos) { sscanf(answer.c_str(), "%f", &result); }
    return result;
}

void RigolChannel::setOverVoltageProtection(float maxVoltage) { write(":OUTP:OVP:VAL " + fChannel + "," + std::to_string(maxVoltage)); }

void RigolChannel::setOverCurrentProtection(float maxCurrent) { write(":OUTP:OVC:VAL " + fChannel + "," + std::to_string(maxCurrent)); }

float RigolChannel::getOverVoltageProtection()
{
    std::string answer = read(":OUTP:OVP:VAL? " + fChannel);
    float       result = 0;
    if(answer.rfind("OFF") == std::string::npos) { sscanf(answer.c_str(), "%f", &result); }
    return result;
}

float RigolChannel::getOverCurrentProtection()
{
    std::string answer = read(":OUTP:OVC:VAL? " + fChannel);
    float       result = 0;
    if(answer.rfind("OFF") == std::string::npos) { sscanf(answer.c_str(), "%f", &result); }
    return result;
}

void RigolChannel::setParameter(std::string parName, float value) { write(parName + " " + std::to_string(value)); }

void RigolChannel::setParameter(std::string parName, bool value) { write(parName + " " + std::to_string(value)); }

void RigolChannel::setParameter(std::string parName, int value) { write(parName + " " + std::to_string(value)); }

float RigolChannel::getParameterFloat(std::string parName)
{
    std::string answer = read(parName);
    float       result;
    sscanf(answer.c_str(), "%f", &result);
    return result;
}

int RigolChannel::getParameterInt(std::string parName)
{
    std::string answer = read(parName);
    int         result;
    sscanf(answer.c_str(), "%d", &result);
    return result;
}

bool RigolChannel::getParameterBool(std::string parName)
{
    std::string answer = read(parName);
    int         result;
    sscanf(answer.c_str(), "%d", &result);
    return result;
}
