/*!
 * \author Martin Delcourt <martin.delcourt@cern.ch>, Vrije Universiteit Brussels
 * \date March 09 2023
 */
#ifndef IIHE_PSU_H
#define IIHE_PSU_H
#define IIHE_ZMQ_BUFFER_SIZE 500

#include "PowerSupply.h"
#include "PowerSupplyChannel.h"
#include <string>

class ZMQConnectionBase
{
  public:
    ZMQConnectionBase(const std::string& host, int port);
    ZMQConnectionBase();
    ~ZMQConnectionBase();
    std::string request(const std::string& command);
    bool        isConnected();
    bool        connect(const std::string& host, int port);

  private:
    char        fBuffer[IIHE_ZMQ_BUFFER_SIZE];
    void*       fContext;
    void*       fRequester;
    bool        fIsConnected;
    std::string fHost;
    int         fPort;
};

class ZMQConnection
{
  public:
    ZMQConnection(const std::string& host, int port);
    ~ZMQConnection();
    std::string request(const std::string& command);
    bool        isConnected();

  private:
    bool              fIsConnected;
    ZMQConnectionBase fConnection;
};

class IIHEPowerSupply : public PowerSupply
{
  public:
    IIHEPowerSupply(const pugi::xml_node configuration);
    virtual ~IIHEPowerSupply();
    void configure();

    // bool turnOn();
    // bool turnOff();
  private:
    ZMQConnection* fConnection;
};

class IIHEPowerSupplyChannel : public PowerSupplyChannel
{
  public:
    IIHEPowerSupplyChannel(ZMQConnection* connection, const pugi::xml_node configuration);
    ~IIHEPowerSupplyChannel();

    // bool  reset() override;
    // bool  isOpen() override;
    void turnOn(void) override;
    void turnOff(void) override;
    bool isOn(void) override;
    // bool  setVoltageMode() override;
    // bool  setCurrentMode() override;
    // bool  setVoltageRange(float voltage) override;
    // bool  setCurrentRange(float current) override;
    void setVoltage(float voltage) override;
    void setCurrent(float current) override;
    void setVoltageCompliance(float voltage) override;
    void setCurrentCompliance(float current) override;
    void setOverVoltageProtection(float voltage) override;
    void setOverCurrentProtection(float current) override;

    float getSetVoltage() override;
    float getOutputVoltage() override;
    float getCurrent() override;
    float getVoltageCompliance() override;
    float getCurrentCompliance() override;
    float getOverVoltageProtection() override;
    float getOverCurrentProtection() override;
    void  setParameter(std::string parName, float value) override;
    void  setParameter(std::string parName, bool value) override;
    void  setParameter(std::string parName, int value) override;
    float getParameterFloat(std::string parName) override;
    int   getParameterInt(std::string parName) override;
    bool  getParameterBool(std::string parName) override;

  private:
    ZMQConnection* fConnection;
    std::string    fChannelCommand;

    void                     write(std::string);
    std::string              read(std::string);
    std::string              fPrefix;
    std::string              fChannelId;
    std::vector<std::string> getStatus();
};

#endif
