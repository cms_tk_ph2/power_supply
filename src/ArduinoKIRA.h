/*!
 * \author Stefan Maier <s.maier@kit.edu>, ETP-Karlsruhe
 * \date Apr 30 2020
 */
#ifndef ARDUINOKIRA_H
#define ARDUINOKIRA_H

#include "Arduino.h"
#include "KIRALED.h"
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

class ArduinoKIRA : public Arduino
{
  public:
    ArduinoKIRA(const pugi::xml_node configuration);
    virtual ~ArduinoKIRA();
    void configure();

    void setParameter(std::string parameter, float value, std::string component = "") override;
    // void setParameter(std::string parameter, bool value,        std::string component = "") override;
    void setParameter(std::string parameter, int value, std::string component = "") override;
    void setParameter(std::string parameter, std::string value, std::string component = "") override;

    float       getParameterFloat(std::string parameter, std::string component = "") override;
    int         getParameterInt(std::string parameter, std::string component = "") override;
    bool        getParameterBool(std::string parameter, std::string component = "") override;
    std::string getParameterString(std::string parameter, std::string component = "") override;

    std::vector<std::string> getStatusList(void) const override { return fStatusList; }
    std::vector<std::string> getComponentList(void) const override { return fLEDList; }

  protected:
    std::unordered_map<std::string, KIRALED*> fLEDMap;
    std::vector<std::string>                  fLEDList;

  private:
    Connection*              fConnection;
    std::vector<std::string> fStatusList{"temperature", "humidity", "dewpoint"};
    void                     sendCommand(std::string command);

    KIRALED*                 getLED(const std::string& id);
    std::vector<std::string> getLEDList(void) const;
};

#endif

/*!
************************************************
Command list to control/readout the KIRA Arduino
************************************************
Set frequency to 1000Hz:
  frequency_1000

Set DAC value range for LEDs high or low
  dacLed_high / dacLed_low

Set pulse length of the trigger signal (DAC value)
  pulseLength_800

Turn trigger on or off:
  trigger_on / trigger_off

Readout request for temperature, humidity, ...
  temperature
  humidity
  dewpoint
  trigger
  frequency
  dacLed
  pulseLength

Set intensity of an LED (1) to a value 30000
  intensity_01_30000

*/